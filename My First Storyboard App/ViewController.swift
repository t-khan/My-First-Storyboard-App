//
//  ViewController.swift
//  My First Storyboard App
//
//  Created by Tofik Khan on 4/26/21.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var textField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // Make the text input active on Load
        textField.becomeFirstResponder()
        
        // Allow users to close keyboard by tapping anywhere other than the keyboard
        
    }

    @IBAction func buttonPressed(_ sender: Any) {
        label.text = "Hello \(textField.text!)!"
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        textField.resignFirstResponder()
    }
}

